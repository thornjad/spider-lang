# Spider

> What if everything was spiders?

&mdash; [Reddit user emmyyyy](https://www.reddit.com/r/AskReddit/comments/2t73j8/what_if_everything_was_spiders/)

Spider is an esoteric programming language, pulling inspiration from many sources, with the notable influence of [a legendary post on r/AskReddit](https://www.reddit.com/r/AskReddit/comments/2t73j8/what_if_everything_was_spiders/). This repository contains an informal definition of the language, as well a naïve interpreter.

## Contents:

- [Informal definition]('./spiders.md')
- Interpreters:
  - [OCaml (coming soon)]('./src/spider.ml')

## License

Spider and its accompanying programs are free software under the terms of the ISC License. See [LICENSE](./LICENSE) for details.
